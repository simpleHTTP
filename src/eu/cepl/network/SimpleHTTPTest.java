package eu.cepl.network;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.prefs.Preferences;

import javax.naming.AuthenticationException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.monkey.nelson.io.BrokenInputStream;
import org.monkey.nelson.testurl.TestURLRegistry;

public class SimpleHTTPTest {

    private static final String GoogleLoginURL = "https://www.google.com/accounts/ClientLogin";
    private static final String feedURL = "http://www.blogger.com/feeds/default/blogs";
    private static final String testDataString = "Byl pozdní večer první máj!\n"
            + "Нас было много на челне;\nИные парус напрягали,\nДругие дружно упирали\n"
            + "В глубь мощны веслы. В тишине\nНа руль склонясь, наш кормщик умный\nВ "
            + "молчаньи правил грузный чолн;\nА я — беспечной веры полн —\nПловцам я пел....";
    private static final String testFileName = "test/Wave.svg";
    private static final int MAX_BYTE_ARRAY = 100000;

    @Before
    public void setupTestURLs() throws Exception {
        InputStream stringIS = new ByteArrayInputStream(testDataString
                .getBytes());
        InputStream imageIS = null;
        imageIS = new FileInputStream(new File(testFileName));
        TestURLRegistry.register("machaPuskin", stringIS);
        TestURLRegistry.register("dukeAI", imageIS);

        // Special inputStream which fails
        // when read more than 10 bytes it will throw an IOException.
        // saved for future testing, not used now
        int bytesToRead = 10;
        @SuppressWarnings("unused")
        BrokenInputStream testStringIS = new BrokenInputStream(stringIS,
                bytesToRead);

    }

    @Test
    public final void virtualTestSimpleHTTPGet() throws IOException {
        SimpleHTTP testURL = new SimpleHTTP(new URL("testurl:machaPuskin"));
        Assert.assertNotNull(testURL);
        String caughtString = testURL.get();
        Assert.assertEquals(testDataString, caughtString);
    }

    @Test
    public final void virtualTestSimpleHTTPGetBinary() throws IOException {
        SimpleHTTP testURL = null;

        testURL = new SimpleHTTP(new URL("testurl:dukeAI"));
        Assert.assertNotNull(testURL);

        byte[] caughtByteArray = new byte[MAX_BYTE_ARRAY];
        BufferedInputStream caughtIS = testURL.getInputStream(null);
        int caughtReadCount = caughtIS.read(caughtByteArray);
        caughtIS.close();

        byte[] expectedByteArray = new byte[MAX_BYTE_ARRAY];
        BufferedInputStream expectedIS = new BufferedInputStream(
                new FileInputStream(testFileName));
        int expectedReadCount = expectedIS.read(expectedByteArray);
        expectedIS.close();

        Assert.assertEquals(expectedReadCount, caughtReadCount);

        Assert.assertTrue(Arrays.equals(expectedByteArray, caughtByteArray));
    }

    @Test
    public final void yahooTestSimpleHTTPBufReader() throws IOException {
        SimpleHTTP yahoo = new SimpleHTTP("http://www.yahoo.com/");
        BufferedReader in = new BufferedReader(new InputStreamReader(yahoo
                .getInputStream(null), "UTF-8"));

        String outString = "";
        String inputLine = "";
        while ((inputLine = in.readLine()) != null)
            outString += inputLine + "\n";

        Assert.assertTrue(outString.length() > 0);
    }

    @Test
    public final void yahooTestSimpleHTTPReader() throws IOException {
        SimpleHTTP yahoo = new SimpleHTTP("http://www.yahoo.com/");
        BufferedReader in = new BufferedReader(new InputStreamReader(yahoo
                .getInputStream(null), "UTF-8"));

        String outString = "";
        String inCh = "";
        while ((inCh = in.readLine()) != null)
            outString += inCh;

        Assert.assertTrue(outString.length() > 0);
    }

    @Test
    public final void yahooTestSimpleHTTPGet() throws IOException {
        String content = new SimpleHTTP("http://www.yahoo.com/").get();
        Assert.assertTrue(content.length() > 0);
    }

    @Test
    public final void yahooTestCheckResponseCode() throws Exception {
        SimpleHTTP req = new SimpleHTTP("http://www.ceplovi.cz/");
        @SuppressWarnings("unused")
        String content = req.get();
        Assert.assertEquals(HttpURLConnection.HTTP_OK, req.getResponseCode());

        req = new SimpleHTTP("http://www.ceplovi.cz/testFailureSimpleHTTP");
        content = req.get();
        Assert.assertEquals(HttpURLConnection.HTTP_NOT_FOUND, req
                .getResponseCode());
    }

    /**
     * Get ClientLogin token from Google Blogger.
     * 
     * @see {@link http
     *      ://code.google.com/apis/accounts/docs/AuthForInstalledApps.html}
     * 
     * @return String ClientLogin token
     * @throws IOException
     */
    private String googleLogin(SimpleHTTP poster, Preferences prefs)
            throws IOException {
        String logname = prefs.get("login", "");
        String passwd = prefs.get("password", "");
        if (logname.length() == 0) { // TODO testing of environment should be
            // separated
            throw new RuntimeException(
                    "Missing login in the system preferences");
        }

        Map<String, String> loginHeaders = new HashMap<String, String>();
        loginHeaders.put("accountType", "HOSTED_OR_GOOGLE");
        loginHeaders.put("Email", logname);
        loginHeaders.put("Passwd", passwd);
        loginHeaders.put("service", "blogger");
        loginHeaders.put("source", "none-mcepl-simpleHTTPTest");

        String result = poster.post(loginHeaders);

        int respCode = 0;
        if ((respCode = poster.getResponseCode()) != HttpURLConnection.HTTP_OK) {
            Assert.fail("Login to Google failed (response code " + respCode
                    + ")");
        }

        String[] tmpSplit;
        String token = "";
        String[] splitted = result.split(System.getProperty("line.separator"));

        for (String spl : splitted) {
            tmpSplit = spl.split("=", 2);
            if (tmpSplit[0].equals("Auth")) {
                token = tmpSplit[1];
            }
        }
        return token;
    }

    @Test
    public final void googleLoginTestSimpleHTTPPost() throws IOException,
            AuthenticationException {
        Preferences prefs = Preferences.userRoot().node(
                "eu.cepl.postBlog.PostBlog");

        String token = googleLogin(new SimpleHTTP(GoogleLoginURL), prefs);
        Assert.assertTrue("Not having Google token!", token.length() > 0);

        Map<String, String> postHeaders = new HashMap<String, String>();
        postHeaders.put("GData-Version", "2");
        postHeaders.put("Authorization", "GoogleLogin auth=" + token);
        int respLen = new SimpleHTTP(feedURL).get(postHeaders).length();

        Assert.assertTrue("response.length = " + respLen, respLen > 0);
    }

}