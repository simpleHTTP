package eu.cepl.network;

/*
 * TODO:
 * - I have to figure out how to switch on logging for HTTP protocol
 * - Location:
 * - Cookies!
 */
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class SimpleHTTP {

    private URL url = null;
    private BufferedInputStream inBS = null;
    private BufferedOutputStream outBS = null;
    // private ConsoleHandler hand = new ConsoleHandler();
    private URLConnection conn = null;
    private int responseCode = 0;

    final static int BUFMAX = 1024;

    // private static Logger logger =
    // Logger.getLogger(SimpleHTTP.class.getName());
    /**
     *
     *
     */
    public SimpleHTTP() {
        // logger.addHandler(hand);
        // hand.setFormatter(new ReallySimpleFormatter());
        //
        // if (new Boolean(System.getProperty("debugging"))) {
        // hand.setLevel(Level.FINE);
        // logger.setLevel(Level.FINE);
        // } else {
        // hand.setLevel(Level.INFO);
        // logger.setLevel(Level.INFO);
        // }
    }

    /**
     * 
     * @param inURL
     */
    public SimpleHTTP(URL inURL) {
        init(inURL);
    }

    /**
     * 
     * @param strURL
     * @throws MalformedURLException
     */
    public SimpleHTTP(String strURL) throws MalformedURLException {
        init(new URL(strURL));
    }

    /**
     * 
     * @param initURL
     */
    private void init(URL initURL) {
        url = initURL;
    }

    /**
     * @param responseCode
     *            the responseCode to set
     */
    protected void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseCode
     */
    public int getResponseCode() {
        return responseCode;
    }

    /**
     * 
     * @return
     */
    public URL getUrl() {
        return url;
    }

    /**
     * 
     * @param headers
     * @return
     * @throws IOException
     */
    private URLConnection getConnection(Map<String, String> headers)
            throws IOException {
        if (headers == null) {
            headers = new HashMap<String, String>();
        }
        if (conn == null) {
            conn = url.openConnection();
            for (Entry<String, String> e : headers.entrySet()) {
                conn.addRequestProperty(e.getKey(), e.getValue());
            }
        }
        return conn;
    }

    /**
     * Provides {@link BufferedOutputStream} for writing (via POST method) to
     * the URL.
     * 
     * @param headers
     * @return BufferedOutputStream for current URL
     * @throws IOException
     * @see getResponseCode
     * 
     *      When the stream is later closed, {@literal getResponseCode} returns
     *      response code of the executed HTTP request (if our current URL is
     *      HTTP, that is).
     */
    public BufferedOutputStream getWriter(Map<String, String> headers)
            throws IOException {
        try {
            conn = getConnection(headers);
            conn.setDoOutput(true);
            outBS = new BufferedOutputStream(conn.getOutputStream());
        } catch (UnsupportedEncodingException e) {
            // Cannot happen, UTF-8 is correct.
        } catch (IOException e) {
            throw new IOException("Cannot read from the HTTP command results.",
                    e);
        }
        return outBS;
    }

    /**
     * Provides {@link BufferedInputStream} pointing to the URL.
     * 
     * @param headers
     * @return BufferedInputStream for reading the remote content from URL
     * @throws IOException
     * @see getResponseCode
     * 
     *      When the stream is later closed, {@literal getResponseCode} returns
     *      response code of the executed HTTP request (if our current URL is
     *      HTTP, that is).
     */
    public BufferedInputStream getInputStream(Map<String, String> headers)
            throws IOException {
        try {
            conn = getConnection(headers);
            inBS = new BufferedInputStream(conn.getInputStream());
        } catch (IOException e) {
            System.err.println("Cannot create BufferedInputStream for URL "
                    + url.toString());
            e.printStackTrace();
            throw e;
        }
        return inBS;
    }

    /**
     * reads whole stream into string
     * 
     * @param in
     *            incoming byte stream
     * @return String in UTF-8 encoding
     * @throws IOException
     */
    String suck(BufferedInputStream in) throws IOException {
        StringBuffer inBuf = new StringBuffer();
        InputStreamReader inR = null;
        int count = 0;

        try {
            inR = new InputStreamReader(in, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // Cannot happen, UTF-8 is always supported
        }
        char[] cbuf = new char[BUFMAX];

        while ((count = inR.read(cbuf)) != -1) {
            inBuf.append(cbuf, 0, count);
        }

        inR.close();
        if (conn instanceof HttpURLConnection) {
            setResponseCode(((HttpURLConnection) this.conn).getResponseCode());
        }

        return inBuf.toString();
    }

    /**
     * Convenience method for getting {@link String} from the URL with
     * possibility to set request headers.
     * 
     * @param headers
     *            Map<String,String> with the name, value pairs of HTTP request
     *            headers.
     * @return String with the remote content
     * @throws IOException
     */
    public String get(Map<String, String> headers) throws IOException {
        BufferedInputStream inCh;

        if (headers == null) {
            headers = new HashMap<String, String>();
        }

        try {
            inCh = getInputStream(headers);
        } catch (IOException e) {
            throw new IOException("Cannot create Reader for URL "
                    + url.toString(), e);
        }

        return suck(inCh);
    }

    /**
     * The most simple convenience method for getting {@link String} from the
     * remote URL.
     * 
     * @return String with the remote content
     * @throws IOException
     */
    public String get() throws IOException {
        return get(null);
    }

    /**
     * 
     * @param inStream
     * @param headers
     * @param mime
     *            String with MIME type of data in {@literal inStream}
     * @return BufferedInputStream with the data returned from the server
     * @throws IOException
     */
    @SuppressWarnings("unused")
    public BufferedInputStream post(BufferedInputStream inStream,
            Map<String, String> headers, String mime) throws IOException {

        BufferedOutputStream outCh;
        BufferedInputStream inCh;
        long inLen = 0;
        int c = 0;
        byte[] inBuf = new byte[BUFMAX];
        String mimeType = "";

        if (mime == null) {
            mimeType = "application/octet-stream";
        } else {
            mimeType = mime;
        }

        if (headers == null) {
            headers = new HashMap<String, String>();
        }

        if (inLen > 0) {
            headers.put("Content-Type", mimeType);
            headers.put("Content-Length", Long.toString(inLen));
        }

        try {
            outCh = getWriter(headers);
            while ((c = inStream.read(inBuf)) != -1) {
                outCh.write(inBuf);
            }
            outCh.flush();
            outCh.close();
        } catch (IOException e) {
            throw new IOException("Cannot POST to URL " + url.toString(), e);
        }

        if (conn instanceof HttpURLConnection) {
            setResponseCode(((HttpURLConnection) this.conn).getResponseCode());
        }

        try {
            inCh = getInputStream(null);
        } catch (IOException e) {
            throw new IOException("Cannot create Reader for URL "
                    + url.toString(), e);
        }

        return inCh;
    }

    /**
     * The main POST method for the object, expected to be used most. Both
     * accepts data as String and returnes back String as a result.
     * 
     * @param data
     *            String with data to be sent in the body of POST
     * @param headers
     *            Map with pairs of String ... name of the header and the value
     *            of the header.
     * @return String with the result of the calling POST
     * @throws IOException
     */
    public String post(String data, Map<String, String> headers)
            throws IOException {

        return suck(post(new BufferedInputStream(new ByteArrayInputStream(data
                .getBytes("UTF-8"))), headers,
                "application/x-www-form-urlencoded"));
    }

    /**
     * 
     * @param inDict
     * @param headers
     * @return
     * @throws IOException
     */
    public String post(Map<String, String> inDict, Map<String, String> headers)
            throws IOException {
        String strData = "";

        for (Entry<String, String> e : inDict.entrySet()) {
            try {
                strData += URLEncoder.encode(e.getKey(), "UTF-8") + "="
                        + URLEncoder.encode(e.getValue(), "UTF-8") + "&";
            } catch (UnsupportedEncodingException e1) {
                // Never happens, UTF-8 is always supported
            }
        }
        if (strData.charAt(strData.length() - 1) == '&') {
            strData = strData.substring(0, strData.length() - 1);
        }
        return post(strData, headers);
    }

    /**
     * 
     * @param inData
     * @return
     * @throws IOException
     */
    public String post(String inData) throws IOException {
        return post(inData, null);
    }

    /**
     * 
     * @param inDict
     * @return
     * @throws IOException
     */
    public String post(Map<String, String> inDict) throws IOException {
        return post(inDict, null);
    }
}
